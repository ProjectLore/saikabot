﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Saika
{
    public interface Notification {
    };

    class Notifications
    {
        
        MySqlConnection mysqlcon;
        MySqlCommand mysqlcmd;
        string myconstr;
        List<NotificationDaily> ListDaily = new List<NotificationDaily>();
        List<NotificationDated> ListDated = new List<NotificationDated>();

        public Notifications()
        {
            myconstr = "server=127.0.0.1;uid=root;" +
                "pwd=password1;database=saikabot;";
            mysqlcon = new MySqlConnection();
            mysqlcon.ConnectionString = myconstr;
            InitializeDatabase();
        }
    
        public NotificationDated ActiveDatedNotification()
        {
            foreach(NotificationDated note in ListDated)
            {
                if (note.Day == DateTime.Now.Day && note.Month == DateTime.Now.Month && note.Year == DateTime.Now.Year && note.Hour == DateTime.Now.Hour && note.Minute == DateTime.Now.Minute)
                    return note;
            }

            return null;
        }

        public NotificationDaily ActiveDailyNotification()
        {
            foreach (NotificationDaily note in ListDaily)
            {
                if (note.WeekDay == DateTime.Now.DayOfWeek.ToString() && note.Hour == DateTime.Now.Hour && note.Minute == DateTime.Now.Minute)
                    return note;
            }

            return null;
        }

        public bool InitializeDatabase()
        {

            try
            {
                mysqlcon.Open();
                string stm = "SELECT * FROM notifications";
                MySqlCommand mysqlcmd = new MySqlCommand(stm, mysqlcon);
                MySqlDataReader rdr = mysqlcmd.ExecuteReader();

                while (rdr.Read())
                    AddNotificationDated(rdr.GetString(0), rdr.GetString(1), rdr.GetInt32(2), rdr.GetInt32(3), rdr.GetInt32(4), rdr.GetInt32(5), rdr.GetInt32(6), rdr.GetString(7));

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }

            mysqlcon.Close();

            try
            {
                mysqlcon.Open();
                string stm = "SELECT * FROM notifications_daily";
                MySqlCommand mysqlcmd = new MySqlCommand(stm, mysqlcon);
                MySqlDataReader rdr = mysqlcmd.ExecuteReader();

                while (rdr.Read())
                    AddNotificationDaily(rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetInt32(3), rdr.GetInt32(4), rdr.GetString(5));


            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }

            mysqlcon.Close();
            return true;
        }

        public bool AddNotificationDated(string v1, string v2, int v3, int v4, int v5, int v6, int v7, string v8)
        {
            ListDated.Add(new NotificationDated(v1, v2, v3, v4, v5, v6, v7, v8));
            return true;
        }

        public bool AddNotificationDaily(string v1, string v2, string v3, int v4, int v5, string v6)
        {
            ListDaily.Add(new NotificationDaily(v1, v2, v3, v4, v5, v6));
            return true;
        }
    }

    class NotificationDated : Notification
    {
        public string Topic;
        public string Text;
        public int Day;
        public int Month;
        public int Year;
        public int Hour;
        public int Minute;
        public string Run;

        public NotificationDated(string v1, string v2, int v3, int v4, int v5, int v6, int v7, string v8)
        {
            this.Topic = v1; this.Text = v2; this.Day = v3; this.Month = v4; this.Year = v5; this.Hour = v6; this.Minute = v7; this.Run = v8;
        }
    }

    class NotificationDaily : Notification
    {
        public string Topic;
        public string Text;
        public string WeekDay;
        public int Hour;
        public int Minute;
        public string Run;

        public NotificationDaily(string v1, string v2, string v3, int v4, int v5, string v6)
        {
            this.Topic = v1; this.Text = v2; this.WeekDay = v3; this.Hour = v4; this.Minute = v5; this.Run = v6;
        }
    }
}
