﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Timers;

namespace Saika
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        Timer _noteTimer;
        public App()
        {
            startTimer();
        }

        void startTimer()
        {
            _noteTimer = new Timer(1 * 60 * 1000);
            _noteTimer.Elapsed += new ElapsedEventHandler(_noteTimer_Elapsed);
            _noteTimer.Enabled = true;
        }

        static void _noteTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
           
        }
    }
}
