﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using MySql.Data.MySqlClient;

namespace Saika
{
    public class Commands
    {
        public List<VoiceCommand> VList = new List<VoiceCommand>();

        MySqlConnection mysqlcon;
        MySqlCommand mysqlcmd;
        string myconstr;

        public Commands()
        {
            myconstr = "server=127.0.0.1;uid=root;" +
                "pwd=password1;database=saikabot;";
            mysqlcon = new MySqlConnection();
            mysqlcon.ConnectionString = myconstr;
            InitializeDatabase();

        }

        public void InitializeDatabase()
        {
            try
            {
                mysqlcon.Open();
                string stm = "SELECT * FROM commands";
                MySqlCommand mysqlcmd = new MySqlCommand(stm, mysqlcon);
                MySqlDataReader rdr = mysqlcmd.ExecuteReader();

                while (rdr.Read())
                    Add(rdr.GetString(0), rdr.GetString(1));

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }

            mysqlcon.Close();
        }

        public void Add(string commandv, string commandrun)
        {
            VList.Add(new VoiceCommand(commandv, commandrun));
        }

        public void StartCommand(string Vcommand)
        {
            foreach (VoiceCommand vc in VList)
                if (vc.command == Vcommand)
                    Process.Start(vc.run);
        }
    }

    public class VoiceCommand
    {
        public string command;
        public string run;

        public VoiceCommand(string commandv, string commandrun)
        {
            command = commandv; run = commandrun;
        }
    }
}