﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Xceed.Wpf.Toolkit;
using MySql.Data.MySqlClient;
using Microsoft.Win32;
using System.Speech.Recognition;
using System.Speech.Synthesis;

namespace Saika
{

    public partial class MainWindow : Window
    {
        SpeechRecognitionEngine sRec = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));
        ManualResetEvent resetRec;
        Thread threadRec;

        MySqlConnection mysqlcon;
        MySqlCommand mysqlcmd;
        string myconstr;

        Notifications noteList = new Notifications();
        System.Timers.Timer noteTimer;
        MediaPlayer noteSound = new MediaPlayer();
        string noteWeekDay;

        Commands cList = new Commands();

        System.Timers.Timer fade_in;
        System.Timers.Timer fade_out;

        public MainWindow()
        {
            InitializeComponent();

            //prop

            //MySQL connections
            myconstr = "server=127.0.0.1;uid=root;" +
                "pwd=password1;database=saikabot;";
            mysqlcon = new MySqlConnection();
            mysqlcon.ConnectionString = myconstr;

            

            //Note Properties
            noteSound.Open(new Uri("D:\\Projects\\SaikaBot\\Saika\\Saika\\bin\\Release\\Sorry.mp3"));
            noteSound.Volume = 1;
            startTimer();
            initSepTimers();
        }

        private void MainGrid_Initialized(object sender, RoutedEventArgs e)
        {
            threadRec = new Thread(startRecognizer);
            threadRec.Start();
        }
        
        void initSepTimers()
        {
            //fade_in.
        }

        void startTimer()
        {
            noteTimer = new System.Timers.Timer(1 * 45 * 1000);
            noteTimer.Elapsed += new ElapsedEventHandler(noteTimer_Elapsed);
            noteTimer.Enabled = true;
        }
        
        void startRecognizer()
        {

            resetRec = new ManualResetEvent(false);
            Choices sList = new Choices();
            sList.Add(new string[] { "show up", "fade away", "notify", "commands", "back", "execute"});
            foreach (VoiceCommand gotya in cList.VList)
                sList.Add(new string[] { gotya.command });

            GrammarBuilder gb = new GrammarBuilder();
            gb.Culture = new System.Globalization.CultureInfo("en-US");
            gb.Append(sList);
            Grammar gm = new Grammar(gb);

            sRec.LoadGrammar(gm);

            sRec.SpeechRecognized += sRec_SpeechRecognized;
            sRec.SpeechRecognitionRejected += sRec_SpeechRecognitionRejected;

            sRec.SetInputToDefaultAudioDevice();
            sRec.RecognizeAsync(RecognizeMode.Multiple);
            resetRec.WaitOne();

        }

        private void sRec_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                TSVBTopic.Content = "Recording";
                TSVBText.Text = e.Result.Text;
                CommandsProceed(e.Result.Text);
            }));
        }

        private void sRec_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                //TSVBTopic.Content = "Recording";
                //TSVBText.Text = e.Result.Text;
                CommandsProceed(e.Result.Text);
            }));
        }


        void CommandsProceed(string command)
        {
            Dispatcher.Invoke((Action)(() =>
            {
            switch(command)
                {
                    case "fade away":
                        {
                            SetSaikaVisualVisible(false);
                            break;
                        }
                    case "show up":
                        {
                            SetSaikaVisualVisible(true);
                            break;
                        }
                    case "notify":
                        {
                            DisplayNoteEdit(true);
                            break;
                        }
                    case "commands":
                        {
                            DisplayCommandEdit(true);
                            break;
                        }
                    case "back":
                        {
                            DisplayMainCanvas(true);
                            break;
                        }
                    case "execute":
                        {
                            this.Close();
                            break;
                        }
                }

            }));

            cList.StartCommand(command);
        }

        void noteTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.HideNotification();
            this.RetrieveDatedNotification();
            this.RetrieveDailyNotification();
        }

        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {
            //Command1.Visibility = Visibility.Hidden;
        }

        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {
            //this.Visibility = Visibility.Hidden;
        }

        private void LeaveIdleMenu(object sender, MouseEventArgs e)
        {

        }

        private void EnterIdleMenu(object sender, MouseEventArgs e)
        {
            
        }

        void DisplayNoteEdit(bool trigger)
        {
            if (trigger)
            {
                MainCanvas.Visibility = Visibility.Hidden;
                NotificationEditCanvas.Visibility = Visibility.Visible;
            }
        }


        void DisplayCommandEdit(bool trigger)
        {
            if (trigger)
            {
                MainCanvas.Visibility = Visibility.Hidden;
                CommandEditCanvas.Visibility = Visibility.Visible;
            }
        }

        void DisplayMainCanvas(bool trigger)
        {
            if (trigger)
            {
                MainCanvas.Visibility = Visibility.Visible;
                CommandEditCanvas.Visibility = Visibility.Hidden;
                NotificationEditCanvas.Visibility = Visibility.Hidden;
            }
        }

        void SetSaikaVisualVisible(bool trigger)
        {
            if(trigger)
                SaikaGridGroup.Visibility = Visibility.Visible;
            else
                SaikaGridGroup.Visibility = Visibility.Hidden;

        }

        private void GridCollapsed(object sender, RoutedEventArgs e)
        {
            SetSaikaVisualVisible(false);
        }

        private void GridExpanded(object sender, RoutedEventArgs e)
        {
            SetSaikaVisualVisible(true);
        }

        private void GotoNotifications(object sender, RoutedEventArgs e)
        {

        }

        private void GotoMain(object sender, RoutedEventArgs e)
        {
            DisplayMainCanvas(true);
        }
        
        private void HideNotification()
        {

            if (ActiveNotification.Visibility == Visibility.Visible)
                Dispatcher.Invoke((Action)(() =>
                {
                        ActiveNotification.Visibility = Visibility.Hidden;
                }));

        }

        private void RetrieveDailyNotification()
        {

            NotificationDaily currentDaily;

            if ((currentDaily = noteList.ActiveDailyNotification()) != null)
            {
                Dispatcher.Invoke((Action)(() =>
                {


                    ActiveNotification.Visibility = Visibility.Visible;
                    NotificationHeadline.Content = currentDaily.Topic;
                    NotificationInfo.Text = currentDaily.Text;
                    Process.Start(currentDaily.Run);

                }));
            }
        }

        private void RetrieveDatedNotification()
        {

            NotificationDated current;

            if ((current = noteList.ActiveDatedNotification()) != null)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    ActiveNotification.Visibility = Visibility.Visible;
                    NotificationHeadline.Content = current.Topic;
                    NotificationInfo.Text = current.Text;
                    Process.Start(current.Run);
                }));
            }

        }

        private void ProceedNotification(object sender, RoutedEventArgs e)
        {


            try
            {

                mysqlcon.Open();

                switch(PeriodTabgrid.SelectedIndex)
                {
                    case 0:
                        {
                            mysqlcmd = new MySqlCommand();
                            mysqlcmd.Connection = mysqlcon;
                            mysqlcmd.CommandText = "INSERT INTO notifications(topic, text, day, month, year, hour, minute, run) VALUES (@topic, @text, @day, @month, @year, @hour, @minute, @run)";
                            mysqlcmd.Prepare();

                            string param1 = NotificationTopic.Text.ToString();
                            string param2 = new TextRange(Notification_Text.Document.ContentStart, Notification_Text.Document.ContentEnd).Text;
                            int param3 = ChosenDate.Value.Value.Day; int param4 = ChosenDate.Value.Value.Month; int param5 = ChosenDate.Value.Value.Year;
                            int param6 = ChosenDate.Value.Value.Hour; int param7 = ChosenDate.Value.Value.Minute;
                            string param8 = NotificationRun.Text;
                            mysqlcmd.Parameters.AddWithValue("@topic", param1);
                            mysqlcmd.Parameters.AddWithValue("@text", param2);
                            mysqlcmd.Parameters.AddWithValue("@day", param3);
                            mysqlcmd.Parameters.AddWithValue("@month", param4);
                            mysqlcmd.Parameters.AddWithValue("@year", param5);
                            mysqlcmd.Parameters.AddWithValue("@hour", param6);
                            mysqlcmd.Parameters.AddWithValue("@minute", param7);
                            mysqlcmd.Parameters.AddWithValue("@run", param8);
                            noteList.AddNotificationDated(param1, param2, param3, param4, param5, param6, param7, param8);
                            break;
                        }
                    case 1:
                        {
                            mysqlcmd = new MySqlCommand();
                            mysqlcmd.Connection = mysqlcon;
                            mysqlcmd.CommandText = "INSERT INTO notifications_daily(topic, text, weekday, hour, minute, run) VALUES (@topic, @text, @weekday, @hour, @minute, @run)";
                            mysqlcmd.Prepare();

                            string param1 = NotificationTopic.Text.ToString();
                            string param2 = new TextRange(Notification_Text.Document.ContentStart, Notification_Text.Document.ContentEnd).Text;
                            string param3 = noteWeekDay;
                            int param4 = ChosenTime.Value.Value.Hour; int param5 = ChosenTime.Value.Value.Minute;
                            string param6 = NotificationRun.Text;

                            mysqlcmd.Parameters.AddWithValue("@topic", param1);
                            mysqlcmd.Parameters.AddWithValue("@text", param2);
                            mysqlcmd.Parameters.AddWithValue("@weekday", param3);
                            mysqlcmd.Parameters.AddWithValue("@hour", param4);
                            mysqlcmd.Parameters.AddWithValue("@minute", param5);
                            mysqlcmd.Parameters.AddWithValue("@run", param6);
                            noteList.AddNotificationDaily(param1, param2, param3, param4, param5, param6);
                            break;
                        }
                }

                mysqlcmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }

            mysqlcon.Close();
        }

        private void NotificationSH(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ActiveNotification.Visibility == Visibility.Visible)
                noteSound.Play();
        }

        private void WindowClosed(object sender, EventArgs e)
        {
            threadRec.Abort();
        }

        private void ChangeDay(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            switch (Convert.ToInt32(e.NewValue))
            {
                case 1:
                    noteWeekDay = "Monday";
                    break;
                case 2:
                    noteWeekDay = "Tuesday";
                    break;
                case 3:
                    noteWeekDay = "Wednesday";
                    break;
                case 4:
                    noteWeekDay = "Thursday";
                    break;
                case 5:
                    noteWeekDay = "Friday";
                    break;
                case 6:
                    noteWeekDay = "Saturday";
                    break;
                case 7:
                    noteWeekDay = "Sunday";
                    break;
            }

        }


        private void ProceedCommand(object sender, RoutedEventArgs e)
        {
            try
            {
                mysqlcon.Open();

                mysqlcmd = new MySqlCommand();
                mysqlcmd.Connection = mysqlcon;

                mysqlcmd.CommandText = "INSERT INTO commands(command, run) VALUES (@command, @run)";
                mysqlcmd.Prepare();

                string param1 = CommandV.Text.ToString();
                string param2 = CommandRun.Text.ToString();

                mysqlcmd.Parameters.AddWithValue("@command", param1);
                mysqlcmd.Parameters.AddWithValue("@run", param2);

                mysqlcmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }


            mysqlcon.Close();
        }
    }
}
